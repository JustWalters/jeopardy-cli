'use strict';

const repl = require('repl');
const api = require('./j-functions');

const fns = {
	// get new clues
	random: [api.random, ['random'], '(J!) Get random clue'],
	byCategory: [api.byCategory, ['byCategory', 'sameCategory'], '(J!) Get more clues from current category'],

	//work with current clue(s)
	seeCurrent: [api.seeCurrent, ['seeCurrent', 'getCurrent', 'current'], '(J!)'],
	seeAnswer: [api.seeAnswer, ['seeAnswer', 'getAnswer'], '(J!)'],
	seeNext: [api.seeNext, ['seeNext', 'getNext', 'next'], '(J!)'],
	correct: [api.correct, ['correct', 'right'], '(J!)'],
	incorrect: [api.incorrect, ['incorrect', 'wrong'], '(J!)'],
	markInvalid: [api.markInvalid, ['markInvalid'], '(J!)'],

	remaining: [api.remaining, ['remaining', 'seeRemaining'], '(J!)'],
	seeScore: [api.seeScore, ['score', 'seeScore', 'getScore'], '(J!)']
};

const replRunning = repl.start();
for (const name of Object.keys(fns)) {
	const fnInfo = fns[name],
				fn = fnInfo[0], aliases = fnInfo[1], help = fnInfo[2];
	aliases.forEach(a => replRunning.defineCommand(a, {
		help,
		action(arg) {
			console.log(fn(arg));
			this.displayPrompt();
		}
	}));
}
