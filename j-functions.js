'use strict';
Set.prototype.hasClue = function(clue) {
	//TODO: can probably refactor
	return Array.from(this).map(x => x.id).includes(clue.id);
};

const request = require('superagent');
const base = 'http://jservice.io/api';

const seen = new Set();
const unseen = new Set();
const correct = new Set();
const incorrect = new Set();
let current;

exports.seeCurrent = seeCurrent;
exports.seeAnswer = seeAnswer;
exports.seeNext = seeNext;
exports.random = random;
exports.byCategory = byCategory;
exports.correct = right;
exports.incorrect = wrong;
exports.seeScore = seeScore;
exports.markInvalid = markInvalid;
exports.remaining = remaining;

function seeCurrent(prop) {
	if (!current) return 'There is no clue active';

	seen.add(current);
	if (!prop) {
		return `${current.value}: ${current.question} - ${current.category.title}`;
	}
	if (prop.toLowerCase() === 'all') {
		return current;
	}
	return current[prop]; // change to _.get or something
};

function seeAnswer() { return seeCurrent('answer'); };

function seeNext() {
	seen.add(current);
	unseen.delete(current);
	current = Array.from(unseen)[0];
	return seeCurrent();
};

function random() {
	request
	.get(base + '/random')
	.then(data => {
		const body = data.body;
		body.forEach(addToUseen());
		current = body[0];
	})
	.catch(e => {
		console.error(e);
	});
	return 'grabbing';
};

function byCategory(limit) {
	if (!current || !current.category || !current.category.id)
		return 'There is no acceptable clue active';

	request
	.get(`${base}/clues?category=${current.category.id}`)
	.then(data => {
		const body = data.body;
		body.forEach(addToUseen(limit));
		//current = body[0];
	})
	.catch(e => {
		console.error(e);
	});
	return 'grabbing';
};

function right() {
	if (!current) return 'There is no clue active';
	correct.add(current);
}

function wrong() {
	if (!current) return 'There is no clue active';
	incorrect.add(current);
}

function seeScore() {
	let sum = 0;
	correct.forEach(clue => sum += clue.value);
	return sum;
}

function markInvalid(id) {
	if (!id && !current) return 'There is no clue active';
	request
	.post(`${base}/invalid?id=${id || current.id}`)
	.then()
	.catch(e => {
		console.error(e);
	});
};

function remaining() {
	return unseen.size;
}

function addToUseen(limit = 10) {
	return function(clue, i) {
		if (limit > 0 && i >= limit) return;
		if (seen.hasClue(clue) || unseen.hasClue(clue)) return;
		unseen.add(clue);
	};
}
